#!/bin/bash

die () {
    echo
    echo "$*"
    echo
    exit 1
}

if ! [[ -d /var/lib/go-agent ]]; then
  echo "It looks like the agent was not installed via deb/rpm"
  exit -1
fi

if [[ "$(whoami)" != 'go' ]]; then
  echo "Must run this script as the `go` user"
  exit -1
fi

mkdir -p /var/lib/go-agent/config || die "Could not create /var/lib/go-agent/config"
mkdir -p /var/lib/go-agent/run || die "Could not create /var/lib/go-agent/run"

# write out autoregister.properties
(
cat <<EOF
agent.auto.register.key=${GO_EA_AUTO_REGISTER_KEY}
agent.auto.register.environments=${GO_EA_AUTO_REGISTER_ENVIRONMENT}
agent.auto.register.elasticAgent.agentId=${GO_EA_AUTO_REGISTER_ELASTIC_AGENT_ID}
agent.auto.register.elasticAgent.pluginId=${GO_EA_AUTO_REGISTER_ELASTIC_PLUGIN_ID}
EOF
) > /var/lib/go-agent/config/autoregister.properties

# write out server url, and prevent backgrounding
printf "wrapper.app.parameter.100=-serverUrl\nwrapper.app.parameter.101=${GO_EA_SERVER_URL}\n" > /usr/share/go-agent/wrapper-config/wrapper-properties.conf
rm -f /var/lib/go-agent/.agent-bootstrapper.running

mkdir -p /var/go/.ssh
touch /var/go/.ssh/known_hosts
ssh-keygen -F bitbucket.org

if [ $? -ne 0 ]
then
    ssh-keyscan bitbucket.org >> /var/go/.ssh/known_hosts
    chown -R go:go /var/go/.ssh
    chmod 600 /var/go/.ssh/known_hosts
fi

# prevent environment variables from leaking into the agent
unset GO_EA_AUTO_REGISTER_KEY
unset GO_EA_AUTO_REGISTER_ENVIRONMENT
unset GO_EA_AUTO_REGISTER_ELASTIC_AGENT_ID
unset GO_EA_AUTO_REGISTER_ELASTIC_PLUGIN_ID
unset GO_EA_SERVER_URL

/usr/share/go-agent/bin/go-agent console