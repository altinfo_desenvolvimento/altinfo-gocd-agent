FROM fedora

LABEL maitainer="Luiz Fernando Pereira <luizfernandopereira@outlook.com.br>" \
      company="Alternativa Informática"

RUN dnf install https://download.gocd.org/binaries/20.10.0-12356/rpm/go-agent-20.10.0-12356.noarch.rpm -y \
    && dnf install git \
    findutils \
    jq -y \
    && dnf clean all

COPY entrypoint.sh /opt/entrypoint.sh
RUN chmod +x /opt/entrypoint.sh

RUN mkdir -p /var/go && chown -R go:go  /var/go
RUN chown go:go /usr/share/go-agent/wrapper-config/wrapper-properties.conf

USER go
ENTRYPOINT [ "/opt/entrypoint.sh" ]